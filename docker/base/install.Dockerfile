ARG CI_REGISTRY_IMAGE

FROM ${CI_REGISTRY_IMAGE}/liquidsoap-base-opam

LABEL maintainer="Raitis Rolis <raitis.rolis@gmail.com>"

ENV DEBIAN_FRONTEND "noninteractive"

# Build and install liquidsoap
USER liquidsoap

WORKDIR /home/liquidsoap

ARG LIQUIDSOAP_BRANCH=v2.0.4-preview

# for debug to see what package need to install on linux
# RUN opam depext pulseaudio
RUN git clone -b ${LIQUIDSOAP_BRANCH} --single-branch https://github.com/savonet/liquidsoap.git
# optional packages ffmpeg ocurl fdkaac lame taglib soundtouch cry pulseaudio
RUN cd liquidsoap && opam install -y . ffmpeg ocurl fdkaac lame taglib soundtouch cry pulseaudio && opam clean

# house cleaning
RUN sudo rm -rf /var/lib/apt/lists/* \
    && sudo apt clean \
    && sudo apt autoremove
RUN eval $(opam env) && /home/liquidsoap/.opam/default/bin/liquidsoap --version

EXPOSE 1234
ENTRYPOINT ["/home/liquidsoap/.opam/default/bin/liquidsoap"]
CMD ["/home/liquidsoap/config/radio.liq"]
