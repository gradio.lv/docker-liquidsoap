ARG CI_REGISTRY_IMAGE

FROM ${CI_REGISTRY_IMAGE}/liquidsoap-base-linux

LABEL maintainer="Raitis Rolis <raitis.rolis@gmail.com>"

ENV DEBIAN_FRONTEND "noninteractive"

# Create a dedicated user with passwordless sudo
RUN useradd -m liquidsoap; echo 'liquidsoap ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER liquidsoap
WORKDIR /home/liquidsoap

RUN opam init -y --disable-sandboxing && opam update -y && opam install -y depext && opam clean && eval $(opam env)
