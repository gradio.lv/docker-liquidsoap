FROM ubuntu:20.04

LABEL maintainer="Raitis Rolis <raitis.rolis@gmail.com>"

ENV DEBIAN_FRONTEND "noninteractive"

RUN apt -qq -y update && apt -qq -y install --no-install-recommends \
    sudo build-essential m4 opam ca-certificates curl \
        # to be able to build liquidsoap
        git pkg-config autoconf automake libpcre3-dev libfdk-aac-dev libmp3lame-dev libcurl4-gnutls-dev \
        # for ffmpeg
        ffmpeg libavcodec-dev libavdevice-dev libavfilter-dev libavformat-dev libavutil-dev libswresample-dev libswscale-dev \
        # for audio tags \
        libtag1-dev zlib1g-dev \
		# sound processing and bpm
 		libpulse-dev swh-plugins tap-plugins libsoundtouch-dev \
        # jack audio connection kit
        jackd1 zita-njbridge \
        && rm -rf /var/lib/apt/lists/* \
        && apt clean \
        && apt autoremove \
        && ln -s /usr/bin/python3 /usr/bin/python || true
