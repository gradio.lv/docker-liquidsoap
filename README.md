## Stream your audio content easy with latest [liquidsoap 2.1.4](https://github.com/savonet/liquidsoap/releases/tag/v2.1.4)

[![pipeline status](https://gitlab.com/gradio.lv/docker-liquidsoap/badges/master/pipeline.svg)](https://gitlab.com/gradio.lv/docker-liquidsoap/-/commits/master)

---
# links
#### [Gitlab gradio.lv docker-stream](https://gitlab.com/gradio.lv/docker-stream) - Project that glues together [liquidsoap](https://hub.docker.com/repository/docker/rolla/liquidsoap) with [icecast](https://hub.docker.com/repository/docker/rolla/icecast-kh)

#### [Gitlab gradio.lv docker-icecast-kh](https://gitlab.com/gradio.lv/docker-icecast-kh)
#### [Docker hub liquidsoap](https://hub.docker.com/repository/docker/rolla/liquidsoap)
#### [Docker hub icecast-kh](https://hub.docker.com/repository/docker/rolla/icecast-kh)

# docker commands
```bash
# see version
docker run rolla/liquidsoap 'settings.init.force_start.set(true)'

# mount your custom liq config file
docker run --rm -p 1234:1234 -v $(pwd)/config/radio.liq:/home/liquidsoap/config/radio.liq rolla/liquidsoap
```
# build
### docker-compose
```bash
time docker-compose -f docker-compose-build.yml build
```
### docker
```bash
docker build --build-arg LIQUIDSOAP_VERSION=2.1.4 LIQUIDSOAP_DEB_URL=https://github.com/savonet/liquidsoap/releases/download/v2.1.4/liquidsoap_2.1.4-ubuntu-jammy-1_amd64.deb --tag rolla/liquidsoap -f version/Dockerfile .
```

---
## Adding music
Copy your files/folders to [music](content/music) directory

---
#### To start with logs
```bash
docker-compose up
```

#### To start in background without logs
```bash
docker-compose up -d
```

# liquidsoap dockerfile defaults

| KEY              | VALUE |
| :--------------: | :---------------------: |
| ENTRYPOINT | `liquidsoap` |
| CMD | `/home/liquidsoap/config/radio.liq` |

```bash
# to override
docker run --rm --entrypoint sh rolla/liquidsoap -c "sleep 10"
```

# tricks
If you have debian with pulseaudio you can listen audio output from docker to your speakers
```bash
docker run --rm -v $(pwd)/content:/home/liquidsoap/content -v $(pwd)/config/radio.liq:/home/liquidsoap/config/radio.liq -v /run/user/1000/pulse/native:/run/user/1000/pulse/native -e PULSE_SERVER=unix:/run/user/1000/pulse/native rolla/liquidsoap
```

access telnet with history
```bash
rlwrap nc -q 1 localhost 1234
```
