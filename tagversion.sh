#!/bin/bash

set -e

# rolla - goes to docker hub
export CI_REGISTRY_IMAGE=rolla

LIQUIDSOAP_VERSION="2.1.4"

LIQUIDSOAP_IMAGE="${CI_REGISTRY_IMAGE}/liquidsoap"
LIQUIDSOAP_CONTAINER="rolla-liquidsoap"

echo ""
echo "Current $0 LIQUIDSOAP_VERSION: ${LIQUIDSOAP_VERSION}"
echo "Current liquidsoap docker-compose-build.yml: $(cat docker-compose-build.yml | grep -m 1 LIQUIDSOAP_VERSION)"
echo ""
echo "Update $0 and docker-compose-build.yml LIQUIDSOAP_VERSION argument to latest versions"
echo ""
read -p "Restart this script and press enter to continue"
echo ""

echo "Removing liquidsoap containers and images"
docker rm -f ${LIQUIDSOAP_CONTAINER} || true
docker rmi -f ${LIQUIDSOAP_IMAGE}:${LIQUIDSOAP_VERSION}
docker rmi -f ${LIQUIDSOAP_IMAGE}:latest

echo "Cleaning system"
echo "y" | docker system prune

echo "Building new image"
time docker-compose -f docker-compose-build.yml build

docker run --rm ${LIQUIDSOAP_IMAGE}:${LIQUIDSOAP_VERSION} --version

# export DOCKER_BUILDKIT=1
#
# docker build --build-arg LIQUIDSOAP_VERSION=2.1.4 --build-arg LIQUIDSOAP_DEB_URL=https://github.com/savonet/liquidsoap/releases/download/v2.1.4/liquidsoap_2.1.4-ubuntu-jammy-1_amd64.deb --tag rolla/liquidsoap:2.1.4 -f version/Dockerfile .
# docker run --rm -it --entrypoint="" rolla/liquidsoap:2.0.0 bash

# to build latest from github
# docker build -t rolla/liquidsoap-base-linux -f docker/base/linux.Dockerfile .
# docker build --build-arg CI_REGISTRY_IMAGE=docker.io/rolla -t rolla/liquidsoap-base-opam -f docker/base/opam.Dockerfile .
# docker build --build-arg CI_REGISTRY_IMAGE=docker.io/rolla --build-arg LIQUIDSOAP_BRANCH=v2.0.4-preview -t rolla/liquidsoap-base-install:2.0.4-preview -f docker/base/install.Dockerfile .
# docker build --build-arg CI_REGISTRY_IMAGE=docker.io/rolla --build-arg LIQUIDSOAP_TAG=2.0.4-preview -t rolla/liquidsoap:2.0.4-preview -f docker/latest/Dockerfile .

echo ""
echo "Test your image with: docker-compose up"
echo ""
echo "Now you can push to docker hub"
echo ""
echo "liquidsoap push and retag:"
echo "docker push ${CI_REGISTRY_IMAGE}/liquidsoap:${LIQUIDSOAP_VERSION}"
echo "retag as latest"
echo "docker tag ${CI_REGISTRY_IMAGE}/liquidsoap:${LIQUIDSOAP_VERSION} ${CI_REGISTRY_IMAGE}/liquidsoap:latest"
echo "docker push ${CI_REGISTRY_IMAGE}/liquidsoap:latest"
echo ""
